package src;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class UsernameServer {
	private String query;
	private int port = 4948;

	public UsernameServer() {
		Thread connectThread = new Thread(new NewConnection());
		connectThread.start();
	}

	private class NewConnection implements Runnable {
		public void run() {
			Socket socket;
			try {
				ServerSocket serverSocket = new ServerSocket(port); //TODO: Fix possible leak!
				while (true) {
					socket = serverSocket.accept();
					new Thread(new TalkToClient(socket)).start();
					System.out.println(socket.getInetAddress().getHostAddress() + " connected");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private class TalkToClient implements Runnable {
		private Socket socket;

		public TalkToClient(Socket socket) {
			this.socket = socket;
		}

		public void run() {
			DBHandler db = new DBHandler();
			try {
				//Receive query
				ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
				ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
				query = input.readUTF();
				
				
				db.writeNewUser(query);
				output.writeUTF(query);
				output.flush();
			} catch (Exception e) {
				e.printStackTrace();
			}

			//Close connection
			try {
				socket.close();
				System.out.println(socket.getInetAddress().getHostAddress() + " disconnected");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	public static void main(String[] args) {
		System.out.println("Starting server");
		UsernameServer tcp = new UsernameServer();
	}
}
